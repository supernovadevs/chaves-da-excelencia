using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public abstract class VsnArgument{

  public virtual float GetNumberValue(){
    return 0f;
  }

  public virtual string GetStringValue(){
    return "";
  }

  public virtual string GetReference(){
    return "";
  }
}

