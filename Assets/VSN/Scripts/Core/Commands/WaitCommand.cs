﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Command{

	[CommandAttribute(CommandString="wait")]
	public class WaitCommand : VsnCommand {

		public override void Execute (){
			VsnController.instance.state = ExecutionState.WAITING;
			VsnController.instance.StartCoroutine(Wait());
		}

		private IEnumerator Wait(){
      yield return new WaitForSecondsRealtime(args[0].GetNumberValue());
			VsnController.instance.state = ExecutionState.PLAYING;
    }

    public override void AddSupportedSignatures() {
      signatures.Add(new VsnArgType[] {
        VsnArgType.numberArg
      });
    }
	}
}