﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Text.RegularExpressions;
using System;
using System.Reflection;
using Command;

public enum ExecutionState {
  STARTING,
  PLAYING,
  WAITING,
  WAITINGTOUCH,
  WAITINGCHOICE,
  WAITINGTEXTINPUT,
  WAITINGCUSTOMINPUT,
  PAUSED,
  STOPPED,
  NumberOfExecutionStates
}


public class VsnController : MonoBehaviour {

  public static VsnController instance;
  public List<Type> possibleCommandTypes;
  public ExecutionState state;
  public GameObject inputBlocker;
  public GameObject vsnCanvas;

  public bool clearGraphicsWhenStop = true;

  public List<VsnScriptReader> scriptsStack;
  public List<VsnScriptReader> nextScripts;

  void Awake() {
    if(instance == null) {
      instance = this;
      possibleCommandTypes = GetClasses("Command");
      scriptsStack = new List<VsnScriptReader>();
      nextScripts = new List<VsnScriptReader>();
    }
  }


  /// <summary>
  /// Starts VSN with a given script path, starting from Resources root.
  /// </summary>
  /// <param name="scriptPath">Script path from Resources root (e.g \"VSN Scripts/myscript.txt\"</param>
  public void StartVSN(string scriptPath, VsnArgument[] args = null) {
    TextAsset textAsset = Resources.Load<TextAsset>(scriptPath);
    if(textAsset == null){
      Debug.LogWarning("Error loading VSN Script: " + scriptPath + ". Please verify the provided path.");
      return;
    }

    if(state == ExecutionState.STOPPED) {
      StartVSNContent(textAsset.text, scriptPath, args);
    } else {
      nextScripts.Add(new VsnScriptReader());
      nextScripts[nextScripts.Count-1].LoadScriptContent(textAsset.text, scriptPath, args);
    }
  }

  public void StartVSNContent(string content, string scriptPath, VsnArgument[] args = null){
    if(state == ExecutionState.STOPPED) {
      CleanVsnElements();

      scriptsStack.Add(new VsnScriptReader());
      CurrentScriptReader().LoadScriptContent(content, scriptPath, args);
      ResumeVSN();
    } else {
      nextScripts.Add(new VsnScriptReader());
      nextScripts[nextScripts.Count-1].LoadScriptContent(content, scriptPath, args);
    }
  }

  void CleanVsnElements(){
    vsnCanvas.SetActive(true);
    BlockExternalInput(true);
    VsnUIManager.instance.SetTextTitle("");
    VsnUIManager.instance.ShowSkipButton(false);
    SelectUiElement(null);
  }

  public void GotoVSNScript(string scriptPath, VsnArgument[] args){
    TextAsset textAsset = Resources.Load<TextAsset>(scriptPath);
    if(textAsset == null){
      Debug.LogWarning("Error loading VSN Script: " + scriptPath + ". Please verify the provided path.");
      return;
    }
//    CurrentScriptReader().currentCommandIndex++;
    PauseVSN();
    scriptsStack.Add(new VsnScriptReader());
    CurrentScriptReader().LoadScriptContent(textAsset.text, scriptPath, args);
    ResumeVSN();
  }

  public VsnScriptReader CurrentScriptReader(){
    return scriptsStack[scriptsStack.Count-1];
  }


  IEnumerator ExecuteScript() {
    VsnScriptReader reader = CurrentScriptReader();
    while(!reader.HasFinished()){
      reader.ExecuteCurrentCommand();
      while(state != ExecutionState.PLAYING) {
        yield return null;
      }
    }
    FinishScript();
  }

  public void ResumeVSN() {
    state = ExecutionState.PLAYING;
    CurrentScriptReader().SetArgs();
    StartCoroutine(ExecuteScript());
  }

  public void PauseVSN() {
    state = ExecutionState.PAUSED;
    StopAllCoroutines();
  }

  public void WaitForCustomInput(){
    state = ExecutionState.WAITINGCUSTOMINPUT;
  }

  public void GotCustomInput(){
    if(state == ExecutionState.WAITINGCUSTOMINPUT){
      state = ExecutionState.PLAYING;
    }
  }

  public void FinishScript(){
    Debug.Log("finishing script");
    scriptsStack.RemoveAt(scriptsStack.Count-1);
    if(scriptsStack.Count > 0) {
      Debug.Log("resuming script");
      ResumeVSN();
    } else {
      FinishVSN();
    }

    if(nextScripts.Count > 0) {
      scriptsStack.Add(nextScripts[0]);
      nextScripts.RemoveAt(0);
      CleanVsnElements();
      ResumeVSN();
    }
  }

  void FinishVSN(){
    Debug.LogWarning("Finishing VSN execution!");

    state = ExecutionState.STOPPED;
    if(clearGraphicsWhenStop){
      VsnUIManager.instance.ResetBackground();
      VsnUIManager.instance.ResetAllCharacters();
      VsnUIManager.instance.ShowSkipButton(false);
    }
    BlockExternalInput(false);
  }

  public void BlockExternalInput(bool value){
    if(inputBlocker != null){
      inputBlocker.SetActive(value);
    }
  }


  private List<Type> GetClasses(string nameSpace) {
    Assembly asm = Assembly.GetExecutingAssembly();

    List<Type> typeList = new List<Type>();

    foreach(Type type in asm.GetTypes()) {
      if(type.Namespace == nameSpace)
        typeList.Add(type);
    }

    return typeList;
  }


  public void Update(){
    if(Input.GetKeyDown(KeyCode.KeypadEnter) ||
       Input.GetKeyDown(KeyCode.Return) ||
       Input.GetKeyDown(KeyCode.Space) ||
       Input.GetKeyDown(KeyCode.J)) {
      switch(state){
        case ExecutionState.WAITINGTEXTINPUT:
          if(Input.GetKeyDown(KeyCode.Space)){
            break;
          }
          VsnUIManager.instance.OnTextInputConfirm();
          break;
        case ExecutionState.WAITINGTOUCH:
          VsnUIManager.instance.OnScreenButtonClick();
          break;
      }
    }
  }


  public static void SelectUiElement(GameObject toSelect){
    EventSystem.current.SetSelectedGameObject(toSelect);
  }
}
