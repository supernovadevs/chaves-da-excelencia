﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpecialCodes {

  public static string InterpretStrings(string initialString){
    string currentString = initialString;

    if(initialString == null) {
      return "";
    }

    if(!initialString.Contains("\\")){
      return initialString;
    }

    currentString = ReplaceString(currentString, "\\n", "\n");
    return currentString;
  }

  static string ReplaceString(string initialString, string specialString, string convertTo){
    while(true){
      int index = initialString.IndexOf(specialString);

      if(index == -1){
        break;
      }else{
        int stringSize = initialString.Length;

        if(stringSize > index + specialString.Length){
          initialString = initialString.Substring(0, index) + convertTo + initialString.Substring(index + specialString.Length, stringSize-index- specialString.Length);
        }else{
          initialString = initialString.Substring(0, index) + convertTo;
        }
      }
    };
    return initialString;
  }


  public static float InterpretFloat(string keycode){
    if(!keycode.Contains("#")){
      return 0f;
    }

    return InterpretSpecialInt(keycode);
  }

  static float InterpretSpecialInt(string keycode){
    switch(keycode){
      case "#ios":
        return Application.platform == RuntimePlatform.IPhonePlayer? 1f : 0f;
      default:
        return 0f;
    }
    return 0f;
  }
}
