﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Command {

  [CommandAttribute(CommandString = "reset_pin_button")]
  public class ResetPinButton : VsnCommand {

    public override void Execute() {
      GivePinPanel.instance.RepositionPinButton();
    }

    public override void AddSupportedSignatures() {
      signatures.Add(new VsnArgType[0]);
    }
  }
}