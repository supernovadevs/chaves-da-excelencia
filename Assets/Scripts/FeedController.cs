﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using GameSparks.Api.Requests;
using GameSparks.Api.Responses;

public class FeedController : MonoBehaviour {

	public Transform contentTransform;
  public GameObject feedEntry;
  public GameObject loadingIcon;


  public void Start() {
    UpdateFeed();
  }

  void UpdateFeed(){
    SetLoading(true);
    
    new ListMessageRequest()
    .SetEntryCount(100)
    .Send( (response)=> {
      if (!response.HasErrors) {
        SetLoading(false);
        Debug.Log("List messages success! " + response.JSONString);
        CreateFeedEntries(response);
      } else{
        Debug.LogError("List messages error: " + response.JSONString);
      }
    } );
  }

  public void CreateFeedEntries(ListMessageResponse response){
    GameObject newObj;
    foreach (GameSparks.Core.GSData entry in response.MessageList){
      if (entry.GetString("@class") == ".ScriptMessage") {
        newObj = Instantiate(feedEntry, contentTransform);
        newObj.GetComponentInChildren<FeedEntry>().SetEntryData(entry);
      }
    }
  }

  void CustomizeEntry(){

  }

  public void SetLoading(bool value) {
    loadingIcon.SetActive(value);
  }
}
