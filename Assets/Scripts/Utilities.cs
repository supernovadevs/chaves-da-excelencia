﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public enum ThemeName {
  safety,
  courtesy,
  respect,
  agility
}



public static class Utilities{

	public static string GetThemeNamePortuguese(ThemeName theme){
    switch (theme) {
      case ThemeName.agility:
        return "Agilidade";
      case ThemeName.safety:
        return "Segurança";
      case ThemeName.courtesy:
        return "Cortesia";
      case ThemeName.respect:
        return "Respeito";
    }
    return "";
  }

  public static string GetThemeNamePortuguese(int themeId) {
    return GetThemeNamePortuguese((ThemeName)themeId);
  }

  public static string GetThemeNameUppercase(int i) {
    return GetThemeNameUppercase((ThemeName)i);
  }

  public static string GetThemeNameUppercase(ThemeName theme){
    string basic = theme.ToString();
    return basic.Substring(0, 1).ToUpper() + basic.Substring(1, basic.Length-1);
  }

  public static string GetLeaderboardShortcode(int theme) {
    return GetLeaderboardShortcode((ThemeName)theme);
  }

  public static string GetLeaderboardShortcode(ThemeName theme) {
    return "pin_" + theme.ToString();
  }

  public static string GetFormatedDisplayName(string displayName) {
    if (displayName == "" || displayName == null)
      return "";

    string[] nameParts = displayName.Split(' ');
    string finalString = "";

    foreach(string part in nameParts) {
      if(finalString != "") {
        finalString += " ";
      }
      if(part.Length > 0) {
        if(IsArticle(part)){
          finalString += part.ToLower();
        } else{
          finalString += part.Substring(0, 1).ToUpper() + part.Substring(1, part.Length - 1).ToLower();
        }
      }
    }

    return finalString;
  }

  public static bool IsArticle(string text) {
    switch (text.ToLower()) {
      case "de":
        return true;
      case "do":
        return true;
      case "da":
        return true;
      case "dos":
        return true;
      case "das":
        return true;
    }
    return false;
  }

  public static void UnselectButton() {
    EventSystem.current.SetSelectedGameObject(null);
  }

  public static void SelectUiObject(GameObject obj) {
    EventSystem.current.SetSelectedGameObject(obj);
  }
}
