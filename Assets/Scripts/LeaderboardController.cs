﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class LeaderboardController : MonoBehaviour {

  public LeaderboardEntry[] leaderboardEntries;
  public int startsFrom = 0;
  public Button previousButton;
  public Button fastBackwardButton;
  public Button nextButton;
  public Button fastForwardButton;

  public Button[] pinButtons;

  public GameObject loadingIcon;

  public int selectedTheme = -1;

  private bool showMyPosition;

  static LeaderboardController instance;

	void Awake(){
    ClearLeaderboard();
    instance = this;
	}

  public static LeaderboardController GetInstance(){
    return instance;
  }

  public void OpenLeaderboard(int theme){
    //SoundManager.GetInstance().PlaySfx("OpenMenu");

    startsFrom = 0;
    GetComponent<Panel>().OpenPanel();
    ShowPinLeaderboard(theme);
    Utilities.UnselectButton();

    //VsnController.instance.StartVSN("tutorial_leaderboard");
  }

  public void CloseLeaderboard(){
    //SoundManager.GetInstance().PlaySfx("CloseMenu");
    //gameObject.SetActive(false);
    GetComponent<Panel>().ClosePanel();
  }


  public void ShowPinLeaderboard(int theme){
    //SoundManager.GetInstance().PlayConfirmSound();
    showMyPosition = true;
    startsFrom = 0;
    selectedTheme = theme;
    SetNotInteractive(theme);
    UpdateLeaderboard();
  }

  public void SetNotInteractive(int id){
    foreach(Button b in pinButtons) {
      b.interactable = true;
    }
    pinButtons[id].interactable = false;
  }


  public void ClickNextButton(){
    //SoundManager.GetInstance().PlayConfirmSound();
    IncreaseInitialEntry(10);
    UpdateLeaderboard();
  }

  public void ClickFastForwardButton(){
    //SoundManager.GetInstance().PlayConfirmSound();
    IncreaseInitialEntry(100);
    UpdateLeaderboard();
  }



  public void ClickPreviousButton(){
    //SoundManager.GetInstance().PlayConfirmSound();
    IncreaseInitialEntry(-10);
    UpdateLeaderboard();
  }

  public void ClickFastBackwardButton(){
    //SoundManager.GetInstance().PlayConfirmSound();
    IncreaseInitialEntry(-100);
    UpdateLeaderboard();
  }

  void IncreaseInitialEntry(int reduceBy){
    startsFrom +=reduceBy;
    if(startsFrom<0){
      startsFrom = 0;
    }
  }  



  public void ClearLeaderboard(){
    foreach(LeaderboardEntry le in leaderboardEntries){
      le.Clear();
    }
    previousButton.interactable = false;
    fastBackwardButton.interactable = false;
    nextButton.interactable = false;
    fastForwardButton.interactable = false;
    loadingIcon.SetActive(true);
  }

  public void UpdateLeaderboard(){
    ClearLeaderboard();
    //if(showMyPosition) {
    //  StartCoroutine(GetMyPositionData());
    //} else {
    StartCoroutine(GetGlobalLeaderboardData());
    //}
	}

  public IEnumerator GetGlobalLeaderboardData(){
    Debug.Log("Sending GetLeaderboardData request!");

    new GameSparks.Api.Requests.LeaderboardDataRequest()
      .SetLeaderboardShortCode( Utilities.GetLeaderboardShortcode(selectedTheme) )
      .SetOffset(startsFrom)
      .SetEntryCount(11)
      .Send((response) => {
        if(!response.HasErrors){
          Debug.LogWarning("Got leaderboard data:");
          Debug.Log( response.JSONString );

          List<GameSparks.Core.GSData> usersData = response.BaseData.GetGSDataList("data");
          UpdateLeaderboardEntries(usersData);
        }else{
          Debug.LogError("Error loading leaderboard data!");
        }
      } );
    yield return null;
  }

  public IEnumerator GetMyPositionData(){
    Debug.Log("Sending GetLeaderboardData request!");

    new GameSparks.Api.Requests.AroundMeLeaderboardRequest()
      .SetLeaderboardShortCode(Utilities.GetLeaderboardShortcode(selectedTheme))
      .SetEntryCount(5)
      .Send((response) => {
      if(!response.HasErrors){
        Debug.LogWarning("Got leaderboard data:");
        Debug.Log( response.JSONString );

        List<GameSparks.Core.GSData> usersData = response.BaseData.GetGSDataList("data");
        UpdateLeaderboardEntries(usersData);
      }else{
        Debug.LogError("Error loading leaderboard data!");
        loadingIcon.SetActive(false);
      }
    } );
    yield return null;
  }

  public void UpdateLeaderboardEntries(List<GameSparks.Core.GSData> usersData){
    int rank;
    int entriesToUpdate = Mathf.Min(usersData.Count, 10);

    if(usersData.Count == 0){
      if(startsFrom > 0){
        IncreaseInitialEntry(-10);
        UpdateLeaderboard();
      }else{
        Debug.LogError("NO ENTRY TO SHOW!");
      }
      return;
    }

    for(int i=0; i<entriesToUpdate; i++){
      rank = (int)usersData[i].GetFloat("rank");

      leaderboardEntries[i].UpdateEntry(rank, usersData[i].GetString("userName"),
                                        (int)usersData[i].GetFloat("pins" + Utilities.GetThemeNameUppercase(selectedTheme)) );
    }
    FinishLoadingLeaderboard(usersData.Count);
  }


  public void FinishLoadingLeaderboard(int howManyEntriesLoaded){
    loadingIcon.SetActive(false);

    if(showMyPosition == false) {
      if(howManyEntriesLoaded >= 11){
        nextButton.interactable = true;
        fastForwardButton.interactable = true;
      }
      if(startsFrom > 0){
        previousButton.interactable = true;
        fastBackwardButton.interactable = true;
      }
    }
  }
}
