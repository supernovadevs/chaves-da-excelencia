﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public static class GlobalData {

  public static string gamesparksUserId = null;
  public static string userDisplayName = null;
  public static string userOccupation = null;
  public static string username = null;
  public static string pictureId = null;

  public static int[] pins;
}
