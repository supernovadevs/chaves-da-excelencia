﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class PhotoBufferEntry {
  public string pictureId;
  public Sprite photo;

  public PhotoBufferEntry(string argpictureId, Sprite argphoto) {
    pictureId = argpictureId;
    photo = argphoto;
  }
}


public class PhotosBuffer : MonoBehaviour {

  public static PhotosBuffer instance;

  public PhotoBufferEntry[] buffer;


  public void Awake() {
    if (instance == null) {
      instance = this;
    }
    buffer = new PhotoBufferEntry[41];
  }

  public void BufferPhoto(string pictureId, Sprite photo){
    for(int i=0; i<buffer.Length; i++){
      if(buffer[i] == null) {
        buffer[i] = new PhotoBufferEntry(pictureId, photo);
        return;
      }
    }
  }

  public bool IsPictureBuffered(string pictureId){
    for (int i = 0; i < buffer.Length; i++) {
      if (buffer[i] != null && buffer[i].pictureId == pictureId) {
        return true;
      }
    }
    return false;
  }

  public Sprite GetBufferedPhoto(string pictureId){
    for (int i = 0; i < buffer.Length; i++) {
      if (buffer[i] != null && buffer[i].pictureId == pictureId) {
        return buffer[i].photo;
      }
    }
    return null;
  }
}
