﻿using GameSparks.Api.Requests;
using GameSparks.Api.Messages;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.Collections;
using System.Runtime.InteropServices;


public class PhotoController : MonoBehaviour {

  public static PhotoController instance;
  public Image previewImage;
  public GameObject errorMessageObject;
  public Text errorMessageText;
  public Slider progressBar;

  public Texture2D userPhotoTexture;

  static string s_dataUrlPrefix = "data:image/png;base64,";

  #if UNITY_WEBGL
    [DllImport("__Internal")]
    private static extern void getImageFromBrowser(string objectName, string callbackFuncName);
  #endif


  void Awake(){
    instance = this;
    //PickerEventListener.onImageLoad += OnImageLoadMobile;
    //PickerEventListener.onImageSelect += OnImageSelectMobile;
    //PickerEventListener.onError += OnErrorMobile;
    //PickerEventListener.onCancel += OnCancelMobile;

    UploadCompleteMessage.Listener += GetUploadMessage;
  }

  void Start(){
    LoadPhoto();
  }


  public static PhotoController GetInstance(){
    return instance;
  }

  public void LoadPhoto(){
    Debug.Log("LOAD NEW PHOTO");

    #if UNITY_ANDROID
      AndroidPicker.BrowseImage(false);
    #elif UNITY_IOS
      IOSPicker.BrowseImage(false);
    #elif UNITY_WEBGL
    //if(Screen.fullScreen){
    //  ShowMessage("Enviar fotos não é suportado em modo tela cheia. Por favor pressione Esc para sair do modo tela cheia e escolher sua foto.");
    //}else{
      GetImageFromUserAsync(gameObject.name, "OnImageLoadWebGL");
    //}
    #endif
  }

  static public void GetImageFromUserAsync(string objectName, string callbackFuncName){
    #if UNITY_WEBGL
      getImageFromBrowser(objectName, callbackFuncName);
    #else
      Debug.LogError("Not supported in this platform");
    #endif
  }

  public void OnImageLoadWebGL(string dataUrl) { // USED FOR WEBGL IMAGE LOADING
    if(dataUrl.StartsWith(s_dataUrlPrefix)) {
      byte[] pngData = System.Convert.FromBase64String(dataUrl.Substring(s_dataUrlPrefix.Length));

      Texture2D newTexture = new Texture2D(1, 1);
      if(newTexture.LoadImage(pngData)) {
        userPhotoTexture = newTexture;
        Sprite createdSprite = Sprite.Create(newTexture, new Rect(0, 0, 256, 256),  new Vector2(0.5f, 0.5f), 50f);

        SetPreviewImage(createdSprite);
      } else {
        Debug.LogError("could not decode image");
      }
    } else {
      Debug.LogError("Error getting image:" + dataUrl);
    }
  }

  void SetPreviewImage(Sprite createdSprite){
    previewImage.sprite = createdSprite;
//    if(DashboardManager.GetInstance() != null){
//      DashboardManager.GetInstance().UpdateUserPhoto();
//    }

    GetUploadUrl();
  }



  public void GetUploadUrl(){
    ShowMessage("Enviando foto... Por favor aguarde.");
    ShowProgressBar();

    new GameSparks.Api.Requests.GetUploadUrlRequest()
      .Send((response) =>
      {
        if(!response.HasErrors){
          StartCoroutine( UploadAFile(response.Url) );
        }else{
          Debug.LogError("Error getting Url to upload! " + response.JSONString);
//          ShowMessage("Ocorreu um erro ao enviar sua foto. Por favor, tente novamente mais tarde.");
          HideMessage();
          HideProgressBar();
        }
      });
  }



  public IEnumerator UploadAFile(string uploadUrl){
		Debug.Log("Dimensions w/h: " + userPhotoTexture.width + " / " + userPhotoTexture.height);
		byte[] bytes = userPhotoTexture.EncodeToJPG(50);

    var postForm = new WWWForm();
    postForm.AddBinaryData("file", bytes, "userPhoto.jpg", "image/jpg");

    WWW uploadRequest = new WWW(uploadUrl, postForm);
    float progress = 0f;

    while(progress < 1f){
      progress = uploadRequest.uploadProgress;
      UpdateProgressBar(0.1f+progress*0.8f);
      yield return null;
    }

    // safety wait
    yield return uploadRequest;

    if (uploadRequest.error != null){
      Debug.Log(uploadRequest.error);
    }else{
      Debug.Log(uploadRequest.text);
    }
    yield return null;
  }

  public void GetUploadMessage(GSMessage message){
    string uploadId = message.BaseData.GetString("uploadId");

    //ShowErrorMessage("Enviando foto 2..");

    Debug.LogWarning("Received upload id: " + uploadId);
    SendPlayerPicture(uploadId);
  }

  public void SendPlayerPicture(string pictureId) {
    new GameSparks.Api.Requests.LogEventRequest()
      .SetEventKey("setPlayerPicture")
      .SetEventAttribute("uploadId", pictureId)
      .Send((response) => {
        if (!response.HasErrors) {
          Debug.LogWarning("Updated picture correctly!");
          if (PhotoController.GetInstance() != null) {
            PhotosBuffer.instance.BufferPhoto(pictureId, previewImage.sprite);
            GlobalData.pictureId = pictureId;
            PhotoController.GetInstance().FinishPhotoUpload();
          }
        } else {
          Debug.LogError("Error updating picture! Error: " + response.JSONString);
        }
      });
  }



  public void FinishPhotoUpload(){
//    ShowMessage("Foto enviada com sucesso! Por favor clique em Confirmar para retornar ao jogo.");
    HideMessage();
    PhotoController.GetInstance().HideProgressBar();
    DashboardController.instance.UpdateUserPhoto();
  }  




  public void ShowMessage(string errorMsg){
    errorMessageObject.SetActive(true);
    errorMessageText.text = errorMsg;
  }

  public void HideMessage(){
    errorMessageObject.SetActive(false);
  }


  public void ShowProgressBar(){
    progressBar.gameObject.SetActive(true);
  }

  public void UpdateProgressBar(float progress){
    progressBar.value = progress;
  }

  public void HideProgressBar(){
    progressBar.gameObject.SetActive(false);
    GoBackToGameplay();
  }



  public void GoBackToGameplay(){
    GetComponent<Panel>().ClosePanel();
  }
}
