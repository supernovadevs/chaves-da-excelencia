using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using GameSparks.Api.Requests;
using GameSparks.Api.Responses;
using TMPro;

public class DashboardController : MonoBehaviour {

  public static DashboardController instance;
  public Sprite[] leaderboardIcons;
  public Color[] themeColors;
  
  public Color[] pinPrimaryColors;
  public Color[] pinSecondaryColors;

  public ThemeName selectedPinType;
  public GivePinPanel givePinPanel;
  public Button[] givePinButton;
  public GameObject givePinLoadingIcon;
  public InputField nameInputField;
  public GameObject resetSelectedUserButton;
  public InputField justificationInputField;
  public TextMeshProUGUI givePinTitleText;

  public Panel usersSelectPanel;
  public GameObject userEntryPrefab;
  public Transform userSelectPanelContent;

  public Image givePinPanelImage;
  public Button sendPinButton;

  public GameObject loadingIcon;

  public string selectedUserId = null;
  public GameObject selectedPlayerNameBox;
  public TextMeshProUGUI selectedUserNameText;

  public Image userPhotoImage;
  public TextMeshProUGUI playerNameText;
  public TextMeshProUGUI[] pinCountTexts;

  public Sprite[] pinSprite;
  public Sprite deniedSprite;
  public Sprite approvedSprite;


  public void Awake() {
    instance = this;
    if(GlobalData.username == null) {
      SimpleLogin();
    } else{
      UpdateUI();
    }
  }

  void Start() {
    if (!string.IsNullOrEmpty(GlobalData.pictureId)) {
      LoadPictureAndUpdate(GlobalData.pictureId, userPhotoImage);
    }
  }



  public void LoadPictureAndUpdate(string pictureId, Image imageSlot) {

    if(PhotosBuffer.instance.IsPictureBuffered(pictureId)){
      Debug.LogWarning("PICTURE ID " + pictureId + " IS ALREADY BUFFERED.");
      imageSlot.sprite = PhotosBuffer.instance.GetBufferedPhoto(pictureId);
      return;
    }

    Debug.LogWarning("LOADING PICTURE ID: " + pictureId);

    new GetUploadedRequest()
      .SetUploadId(pictureId)
      .Send((response) => {
        if (!response.HasErrors) {
          Debug.LogWarning("Success loading user photo URL: " + response.Url);
          StartCoroutine(GetUserPhotoAndUpdate(pictureId, response.Url, imageSlot));
        } else {
          Debug.LogError("Error loading user photo URL");
        }
      });
  }

  public IEnumerator GetUserPhotoAndUpdate(string pictureId, string url, Image imageSlot) {
    Debug.Log("Loading user photo from URL: " + url);

    if (PhotosBuffer.instance.IsPictureBuffered(pictureId)) {
      Debug.LogWarning("PICTURE ID " + pictureId + " IS ALREADY BUFFERED.");
      imageSlot.sprite = PhotosBuffer.instance.GetBufferedPhoto(pictureId);
      yield break;
    }

    WWW photoRequest = new WWW(url);
    yield return photoRequest;

    if (photoRequest.error == null || photoRequest.error == "") {
      Debug.LogWarning("Success downloading user photo!");

      if (PhotosBuffer.instance.IsPictureBuffered(pictureId)) {
        Debug.LogWarning("PICTURE ID " + pictureId + " IS ALREADY BUFFERED.");
        imageSlot.sprite = PhotosBuffer.instance.GetBufferedPhoto(pictureId);
        yield break;
      }

      Sprite userSprite = Sprite.Create(photoRequest.texture,
                                        new Rect(0, 0, photoRequest.texture.width, photoRequest.texture.height),
                                        new Vector2(0.5f, 0.5f));
      imageSlot.sprite = userSprite;

      Debug.LogWarning("BUFFERING PICTURE ID " + pictureId + ".");
      PhotosBuffer.instance.BufferPhoto(pictureId, userSprite);
      

    } else {
      Debug.LogError("Error downloading user photo: '" + photoRequest.error + "'");
    }
  }


  void SimpleLogin(){
    Debug.LogWarning("Starting login!");
    new AuthenticationRequest()
    .SetPassword("default")
    .SetUserName("devops")
    .Send( (response)=> {
      Debug.LogWarning("Login Response: " + response.JSONString);

      if (!response.HasErrors) {
        GlobalData.gamesparksUserId = response.UserId;
        GlobalData.userDisplayName = response.DisplayName;
        GlobalData.username = "devops";
        GlobalData.pins = new int[4];
        for(int i=0; i<4; i++){
          GlobalData.pins[i] = response.ScriptData.GetGSData("pins").GetInt("pins" + Utilities.GetThemeNameUppercase(i)).Value;
        }

        if (!string.IsNullOrEmpty(response.ScriptData.GetString("pictureId"))) {
          GlobalData.pictureId = response.ScriptData.GetString("pictureId");
          LoadPictureAndUpdate(GlobalData.pictureId, userPhotoImage);
        }

        Debug.LogWarning("Login success!");
        UpdateUI();
      } else{
        Debug.LogError("Login failed! " + response.Errors.JSON);
      }
    });

    Debug.Log("it was called without problem");
  }

  public void UpdateUI(){
    playerNameText.text = GlobalData.userDisplayName;
    for(int i=0; i<4; i++) {
      pinCountTexts[i].text = GlobalData.pins[i].ToString();
    }
  }

	//public int SelectedPinType(){
 //   for(int i=0; i<4;i++){
 //     if(pinTypeToggles[i].isOn) {
 //       return i;
 //     }
 //   }
 //   return 0;
 // }


  public void ClickGivePinButton(int theme){
    Debug.LogWarning("GIVE PIN!");
    givePinTitleText.text = "Dedicar Pin de " + Utilities.GetThemeNamePortuguese(theme);
    givePinPanelImage.color = pinPrimaryColors[theme];
    selectedPinType = (ThemeName)theme;
    selectedUserId = null;
    CheckSendButtonActive();
    //givePinTitleText.text = "Dar Pin de " ;
    givePinPanel.OpenPinPanel(givePinButton[theme].gameObject);
    justificationInputField.text = "";
    ResetSelectedUser();
  }

  public void ClickCloseGivePinPanelButton(){
    givePinPanel.ClosePanel();
  }

  public void SearchUsers(){
    if(string.IsNullOrEmpty(nameInputField.text.Trim()) ) {
      //TODO error sfx  
      return;
    }

    if(nameInputField.text.Trim().Length < 3) {
      return;
    }

    givePinLoadingIcon.SetActive(true);
    new LogEventRequest()
    .SetEventKey("queryUsersByName")
    .SetEventAttribute("namePart", nameInputField.text.Trim())
    .Send( (response)=> {
      givePinLoadingIcon.SetActive(false);
      if (!response.HasErrors) {
        Debug.Log("Result: " + response.JSONString);
        Debug.LogWarning("No errors!");
        OpenUserSelectPanel(response.ScriptData.GetGSDataList("queryResult"));
      } else{
        Debug.LogError("Has errors! " + response.Errors.JSON);
      }
    } );
  }

  public void ResetSelectedUser(){
    selectedPlayerNameBox.SetActive(false);
    resetSelectedUserButton.SetActive(false);
    selectedUserId = null;
    CheckSendButtonActive();
    nameInputField.text = "";
    Utilities.SelectUiObject(nameInputField.gameObject);
    return;
  }

  public void CheckSendButtonActive(){
    if(selectedUserId != null && !string.IsNullOrEmpty(justificationInputField.text.Trim())){
      sendPinButton.interactable = true;
    }else{
      sendPinButton.interactable = false;
    }
  }


  public void OpenUserSelectPanel(List<GameSparks.Core.GSData> data) {
    CleanUserEntries();
    foreach(GameSparks.Core.GSData gs in data) {
      if(gs.GetGSData("_id").GetString("$oid") != GlobalData.gamesparksUserId) {
        GameObject g = Instantiate(userEntryPrefab, userSelectPanelContent);
        g.GetComponentInChildren<UserSelectEntry>().nameText.text = gs.GetString("displayName");
        g.GetComponentInChildren<UserSelectEntry>().playerId = gs.GetGSData("_id").GetString("$oid");
      }
    }
    //usersSelectPanel.OpenPanel();
  }

  public void CleanUserEntries(){
    for(int i= userSelectPanelContent.childCount-1; i>=0; i--){
      Destroy(userSelectPanelContent.GetChild(i).gameObject);
    }
  }

  public void CloseUserSelectPanel(){
    usersSelectPanel.ClosePanel();
  }

  public void ClickSendButton(){
    ShowLoading(true);

    new LogEventRequest()
    .SetEventKey("pinAwardRequest")
    .SetEventAttribute("pinType", selectedPinType.ToString() )
    .SetEventAttribute("targetPlayerId", selectedUserId)
    .SetEventAttribute("description", justificationInputField.text)
    .Send((response)=> {
      ShowLoading(false);
      if (!response.HasErrors) {
        Debug.Log("Result: " + response.JSONString);
        Debug.LogWarning("No errors!");
        givePinPanel.SendPin();
      } else {
        Debug.LogError("Has errors! " + response.Errors.JSON);
        VsnController.instance.StartVSN("send_pin_fail");
      }
    });
  }

  public void ShowLoading(bool value){
    loadingIcon.SetActive(value);
  }

  public void SelectUser(string playerId, string displayName){
    CleanUserEntries();

    resetSelectedUserButton.SetActive(true);
    selectedPlayerNameBox.SetActive(true);
    selectedUserId = playerId;  
    selectedUserNameText.text = displayName;
    CheckSendButtonActive();
  }

  public void UpdateUserPhoto(){
    userPhotoImage.sprite = PhotosBuffer.instance.GetBufferedPhoto(GlobalData.pictureId);
  }
}
