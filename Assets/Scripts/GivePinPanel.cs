﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class GivePinPanel : Panel {

  public static GivePinPanel instance;

  public GameObject pinPanel;
  public Image blackBgImage;

  private GameObject smallPin;
  private Vector3 initialPosition;
  private Vector3 initialSize;

  public const float rotateTime = 0.5f;
  public const float moveTime = 0.4f;


  public void Awake() {
    instance = this;
  }

  public void OpenPinPanel(GameObject argSmallPin) {
    smallPin = argSmallPin;
    initialPosition = argSmallPin.transform.localPosition;
    initialSize = smallPin.transform.localScale;

    Transform aux = smallPin.transform.parent;
    smallPin.transform.parent = null;
    smallPin.transform.parent = aux;

    pinPanel.transform.eulerAngles = new Vector3(0f, 90f, 0f);
    gameObject.SetActive(true);

    smallPin.transform.DOLocalMove(Vector3.zero, moveTime);
    smallPin.transform.DOScale(Vector3.one, moveTime).OnComplete( ()=> {
      blackBgImage.DOFade(0.5f, rotateTime);
      smallPin.transform.DORotate(new Vector3(0f, -90f, 0f), rotateTime/2f).OnComplete(() => {
        pinPanel.transform.DORotate(Vector3.zero, rotateTime/2f);
      });
    } );
  }

  public override void ClosePanel(){
    blackBgImage.DOFade(0f, rotateTime);
    pinPanel.transform.DORotate(new Vector3(0f, -90f, 0f), rotateTime/2f).OnComplete(()=> {
      smallPin.transform.DORotate(Vector3.zero, rotateTime/2f).OnComplete(() => {
        smallPin.transform.DOLocalMove(initialPosition, moveTime);
        smallPin.transform.DOScale(initialSize, moveTime).OnComplete( ()=> {
          gameObject.SetActive(false);
        }  );
      });
    });
  }

  public void SendPin(){
    //blackBgImage.DOFade(0f, rotateTime);
    //pinPanel.transform.DORotate(new Vector3(0f, -90f, 0f), rotateTime / 2f).OnComplete(() => {
    //  smallPin.transform.DORotate(Vector3.zero, rotateTime / 2f).OnComplete(() => {
    //    //smallPin.transform.DOLocalMove(initialPosition, moveTime);
    //    smallPin.transform.DOScale(initialSize, moveTime).OnComplete(() => {
    //      gameObject.SetActive(false);
    //      VsnController.instance.StartVSN("send_pin_success");
    //    });
    //  });
    //});
    StartCoroutine(SendPinAnimation());
  }

  public IEnumerator SendPinAnimation(){

    pinPanel.transform.DORotate(new Vector3(0f, -90f, 0f), rotateTime / 2f);

    yield return new WaitForSeconds(rotateTime / 2f);

    smallPin.transform.DORotate(Vector3.zero, rotateTime / 2f);

    yield return new WaitForSeconds(rotateTime / 2f);

    smallPin.transform.DOScale(initialSize, moveTime);

    yield return new WaitForSeconds(moveTime+0.2f);

    smallPin.transform.DOLocalMoveX(650f, moveTime);

    yield return new WaitForSeconds(moveTime);
    
    VsnController.instance.StartVSN("send_pin_success");
  }

  public void RepositionPinButton(){
    blackBgImage.DOFade(0f, rotateTime);
    gameObject.SetActive(false);
    smallPin.transform.localPosition = initialPosition;
  }
}
