﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.IO;
using TMPro;

public class LeaderboardEntry : MonoBehaviour {

  public TextMeshProUGUI rankingText;
  public TextMeshProUGUI nameText;
  public TextMeshProUGUI positionText;
  public TextMeshProUGUI scoreText;

  public Color othersColor;
  public Color myColor;

  public void UpdateEntry(int rank, string displayName, int score){
    gameObject.SetActive(true);
    //rankingText.text = rank.ToString();

    nameText.text = "---";
    scoreText.text = score.ToString();
    //positionText.text = Util.GetPositionNameById( Util.GetPositionIdByPoints(score) );
    nameText.text = Utilities.GetFormatedDisplayName(displayName);
    SetColor(displayName);

//    LogEntryToFile(displayName);
  }

  void SetColor(string name){
    if (name == GlobalData.userDisplayName) {
      foreach (Image i in GetComponentsInChildren<Image>()) {
        i.color = myColor;
      }
    } else {
      foreach(Image i in GetComponentsInChildren<Image>()) {
        i.color = othersColor;
      }
    }
  }

  public void Clear(){
    gameObject.SetActive(false);
  }

  public void LogEntryToFile(string name){
    string filePath = Application.dataPath + "/list.txt";

    File.AppendAllText(filePath, name+"\n");
  }
}
