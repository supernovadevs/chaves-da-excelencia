﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class UserSelectEntry : MonoBehaviour {

  public TextMeshProUGUI nameText;
  public string playerId;


  public void Clicked(){
    DashboardController.instance.SelectUser(playerId, nameText.text);
    DashboardController.instance.CloseUserSelectPanel();
  }
}
