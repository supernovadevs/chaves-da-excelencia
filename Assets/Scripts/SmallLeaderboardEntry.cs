﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class SmallLeaderboardEntry : MonoBehaviour {

  public Image photoImage;
  public TextMeshProUGUI nameText;
  public TextMeshProUGUI pinCountText;
}

