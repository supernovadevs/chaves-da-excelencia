﻿using System.Collections;
using System.Collections.Generic;
using GameSparks.Api.Requests;
using GameSparks.Api.Responses;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class SmallLeaderboardController : MonoBehaviour {

  public Color themeColor;
  public ThemeName theme;

  public Image borderImage;
  public Image iconImage;
  public TextMeshProUGUI titleText;
  public SmallLeaderboardEntry[] playerEntries;
  
  public GameObject loadingIcon;
  public GameObject PlayerEntriesPanel;


  void Start () {
    UpdateUI();
    GetLeaderboardsData();
  }
	
	void UpdateUI() {
    Color color = DashboardController.instance.themeColors[(int)theme];

    iconImage.sprite = DashboardController.instance.leaderboardIcons[(int)theme];
    borderImage.color = color;

    titleText.color = color;
    titleText.text = Utilities.GetThemeNamePortuguese(theme);

    Slider[] sliders = transform.GetComponentsInChildren<Slider>();
    foreach(Slider s in sliders){
      s.fillRect.GetComponent<Image>().color = color;
    }
  }

  void GetLeaderboardsData() {
    loadingIcon.SetActive(true);
    PlayerEntriesPanel.SetActive(false);

    List<string> scriptDataArg = new List<string>();
    scriptDataArg.Add("pictureId");

    //Debug.Log("Sending GetLeaderboardsData");
    new LogEventRequest()
    .SetEventKey("LeaderboardWithScriptDataRequest")
    .SetEventAttribute("leaderboardShortCode", Utilities.GetLeaderboardShortcode(theme))
    .SetEventAttribute("entryCount", 3)
    .SetEventAttribute("playerScriptData", scriptDataArg)
    .Send((response) => {
      if (!response.HasErrors) {
        //Debug.LogWarning("Get leaderboard success!");

        Debug.Log("Leaderboard entry data: " + response.JSONString);

        loadingIcon.SetActive(false);
        PlayerEntriesPanel.SetActive(true);
        UpdateValues(response);
      } else {
        Debug.LogWarning("Get leaderboard failed!");
        Debug.LogWarning(response.Errors.JSON);
      }
    });
  }

  void UpdateValues(LogEventResponse leaderboardData){
    List<GameSparks.Core.GSData> usersData = leaderboardData.ScriptData.GetGSData("leaderboardData").GetGSDataList("data");

    for(int i=0; i<3; i++){
      if(i < usersData.Count) {
        playerEntries[i].gameObject.SetActive(true);
        playerEntries[i].nameText.text = usersData[i].GetString("userName");
        playerEntries[i].pinCountText.text = ((int)usersData[i].GetFloat("pins" + Utilities.GetThemeNameUppercase(theme)).Value).ToString();
        if (usersData[i].ContainsKey("pictureId")) {
          DashboardController.instance.LoadPictureAndUpdate(usersData[i].GetString("pictureId"), playerEntries[i].photoImage);
        }
      }else{
        playerEntries[i].gameObject.SetActive(false);
      }

    }
  }
}
