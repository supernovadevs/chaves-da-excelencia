﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using GameSparks.Api.Responses;
using TMPro;

public class FeedEntry : MonoBehaviour {

	public Image notificationIcon;
  public TextMeshProUGUI notificationText;
  public TextMeshProUGUI dateText;

  public void SetEntryData(GameSparks.Core.GSData entry) {
    string type = entry.GetGSData("data").GetString("type");
    int pinTypeId = GetPinTypeId(entry.GetGSData("data").GetString("pin"));
    string sender = entry.GetGSData("data").GetString("senderPlayerDisplayName");
    string receiver = entry.GetGSData("data").GetString("targetPlayerDisplayName");

    string dateString = entry.GetGSData("data").GetString("messageDate");
    dateString = dateString.Substring(0, dateString.Length - 5)+"Z";
    System.DateTime date = System.DateTime.Parse(dateString, null, System.Globalization.DateTimeStyles.RoundtripKind);
    date = date.AddHours(-3);
    dateText.text = System.String.Format("{0:d/M/yyyy HH:mm:ss}", date);
    //Debug.LogWarning("Date: " + dateString);

    switch(type){
      case "pinReceivedNotification":
        notificationText.text = ReceivedPinString(pinTypeId, sender);
        notificationIcon.sprite = DashboardController.instance.pinSprite[pinTypeId];
        break;
      case "pinDeniedNotification":
        notificationText.text = RequestDeniedString(pinTypeId, receiver, entry.GetGSData("data").GetString("denyReason"));
        notificationIcon.sprite = DashboardController.instance.deniedSprite;
        break;
      case "pinRequestApprovedNotification":
        notificationText.text = RequestApprovedString(pinTypeId, receiver);
        notificationIcon.sprite = DashboardController.instance.approvedSprite;
        break;
    }
  }

  public string ReceivedPinString(int pinTypeId, string sender) {
    string pinTypeName = Utilities.GetThemeNamePortuguese(pinTypeId);

    return "Você recebeu um Pin de " + pinTypeName + " de "+sender+".";
  }

  public string RequestDeniedString(int pinTypeId, string receiver, string reason) {
    string pinTypeName = Utilities.GetThemeNamePortuguese(pinTypeId);

    return "Sua requisição de envio do Pin de " + pinTypeName + " para o usuário " + receiver + " foi indeferida. " + reason;
    //return "Sua requisição de envio do Pin de " + pinTypeName + " para o usuário " + receiver + " foi indeferida.";
  }

  public string RequestApprovedString(int pinTypeId, string receiver) {
    string pinTypeName = Utilities.GetThemeNamePortuguese(pinTypeId);

    return "Sua requisição de envio do Pin de " + pinTypeName + " para o usuário " + receiver + " foi deferida.";
  }

  public int GetPinTypeId(string pinType){
    string[] names = System.Enum.GetNames(typeof(ThemeName));
    switch(pinType) {
      case "safety":
        return 0;
      case "courtesy":
        return 1;
      case "respect":
        return 2;
      case "agility":
        return 3;
    }
    return -1;
  }
}
