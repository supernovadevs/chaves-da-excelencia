﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class Panel : MonoBehaviour {

  public void OpenPanel(){
    Open(gameObject);
  }


  public virtual void ClosePanel(){
    Close(gameObject);
  }


  public static void Open(GameObject panel){
    CanvasGroup cv = panel.GetComponent<CanvasGroup>();
    if(cv != null){
      cv.alpha = 0f;
      cv.DOFade(1f, 0.3f);
    }
    panel.SetActive(true);
  }

  public static void Close(GameObject panel){
    CanvasGroup cv = panel.GetComponent<CanvasGroup>();
    if(cv != null) {
      cv.alpha = 1f;
      cv.DOFade(0f, 0.3f).OnComplete(() => {
          panel.SetActive(false);
        });
    } else {
      panel.SetActive(false);
    }
  }
}